# Data file visualize, e.g. for ML feature analysis usage
# It supports to invoke class markings on the data distribution figures
# Currently, it supports CSV file only. The class plots line as well as histogram
# Where the class labels must be non-zero integers and the class column should be named as 'Class'
# Created with Anaconda3 64-bit

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import ntpath
import os


class DataVisualizer(object):
    def __init__(self,
                 csv_file_name,
                 separator=';',
                 decimal='.',
                 col_subplot=4,
                 plot_type=r'raw',
                 mark_class=False):

        self.csv_file_name = csv_file_name
        self.separator = separator
        self.decimal = decimal
        self.data_frame = pd.DataFrame
        self.data_frame_cls_sorted = pd.DataFrame
        self.file_name_no_ext = os.path.splitext(ntpath.basename(self.csv_file_name))[0]
        self.n_col_subplot = col_subplot
        self.plot_type = plot_type
        self.MAX_CLASS_MARKER = 0xFFFF
        self.class_marker = np.zeros((self.MAX_CLASS_MARKER, 3))
        self.class_marker_length = 0
        self.mark_class = mark_class

    def read_csv_from_file(self):

        self.data_frame = pd.read_csv(self.csv_file_name,
                                      decimal=self.decimal,
                                      sep=self.separator)

        for column in self.data_frame.columns:
            self.data_frame[column] = pd.to_numeric(self.data_frame[column], errors='coerce')

    def get_class_marker(self):

        self.read_csv_from_file()
        cls_array = self.data_frame['Class'].to_numpy()
        cls_marker_itr = 0
        cls_start_marker = 0

        for itr in range(1, len(cls_array) - 1):
            if (cls_array[itr] != cls_array[itr + 1] or itr == len(cls_array) - 2) and cls_array[itr] != 0:
                cls_end_marker = itr + 1
                flag = 1
            elif cls_array[itr] != cls_array[itr - 1] and cls_array[itr] != 0:
                cls_start_marker = itr
                flag = 0
            else:
                flag = 0

            if flag == 1:
                self.class_marker[cls_marker_itr][0] = cls_array[itr]
                self.class_marker[cls_marker_itr][1] = cls_start_marker
                self.class_marker[cls_marker_itr][2] = cls_end_marker
                cls_marker_itr += 1
                self.class_marker_length = cls_marker_itr

    def plot_data_line(self):

        if self.mark_class:
            self.get_class_marker()
        else:
            self.read_csv_from_file()

        if self.mark_class:
            local_data_frame = self.data_frame.drop(columns=['Class'])
        else:
            local_data_frame = self.data_frame

        if self.plot_type == r'normalization':
            for column in local_data_frame.columns:
                mean_col = local_data_frame[column].mean()
                std_col = local_data_frame[column].std()
                local_data_frame[column] = (local_data_frame[column] - mean_col) / std_col

        fig, axs = plt.subplots(int(len(local_data_frame.columns) / self.n_col_subplot) + 1, self.n_col_subplot)

        if self.plot_type == r'normalization':
            sup_title = r"Data Distribution Normalization" + '\n' + self.file_name_no_ext
        else:
            sup_title = r"Data Distribution" + '\n' + self.file_name_no_ext
        fig.suptitle(sup_title)

        for column in local_data_frame.columns:
            row_plot_position = int(local_data_frame.columns.get_loc(column) / self.n_col_subplot)
            col_plot_position = int(local_data_frame.columns.get_loc(column) % self.n_col_subplot)
            if self.plot_type == r'normalization':
                axs[row_plot_position, col_plot_position].set_ylabel('Normalization')
                local_data_frame.plot(kind='line',
                                      y=column,
                                      ax=axs[row_plot_position, col_plot_position],
                                      color='red')
            else:
                axs[row_plot_position, col_plot_position].set_ylabel('Data Value')
                local_data_frame.plot(kind='line',
                                      y=column,
                                      ax=axs[row_plot_position, col_plot_position])
            if self.mark_class:
                for cls in range(self.class_marker_length):
                    axs[row_plot_position, col_plot_position].axvspan(self.class_marker[cls][1],
                                                                      self.class_marker[cls][2],
                                                                      facecolor='0.2',
                                                                      alpha=0.3*float(self.class_marker[cls][0]))

            axs[row_plot_position, col_plot_position].set_xlabel('Frame Cycle No.')

        for empty in range((int(len(local_data_frame.columns) / self.n_col_subplot) + 1) * self.n_col_subplot):
            if empty >= int(len(local_data_frame.columns)):
                axs.flat[empty].set_visible(False)
        plt.show()

    def plot_data_hist(self):
        self.read_csv_from_file()
        cls_array = self.data_frame['Class'].to_numpy()
        cls_label = np.array([0])
        flag = 0
        for itr in range(1, len(cls_array)):
            if cls_array[itr] != cls_array[itr - 1]:
                for itr_labels in range(len(cls_label)):
                    if cls_label[itr_labels] == cls_array[itr]:
                        flag = 1
                        break
                    flag = 0
                if flag == 0:
                    cls_label = np.append(cls_label, cls_array[itr])

        fig, axs = plt.subplots(int(len(self.data_frame.columns) / self.n_col_subplot) + 1, self.n_col_subplot)
        sup_title = r"Data Distribution Classification Histogram" + '\n' + self.file_name_no_ext
        fig.suptitle(sup_title)

        for column in self.data_frame.columns.drop(['Class']):
            row_plot_position = int(self.data_frame.columns.get_loc(column) / self.n_col_subplot)
            col_plot_position = int(self.data_frame.columns.get_loc(column) % self.n_col_subplot)
            for itr_labels in range(len(cls_label)):
                label_name = r'Class ' + str(cls_label[itr_labels])
                axs[row_plot_position, col_plot_position].hist(self.data_frame[column][self.data_frame['Class'] == cls_label[itr_labels]], alpha=0.5, label=label_name)
            axs[row_plot_position, col_plot_position].set_ylabel('Frequency')
            data_name = str(column) + r' Value'
            axs[row_plot_position, col_plot_position].set_xlabel(data_name)
            axs[row_plot_position, col_plot_position].legend()

        for empty in range((int(len(self.data_frame.columns.drop(['Class'])) / self.n_col_subplot) + 1) * self.n_col_subplot):
            if empty >= int(len(self.data_frame.columns.drop(['Class']))):
                axs.flat[empty].set_visible(False)
        plt.show()
